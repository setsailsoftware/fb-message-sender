'use strict';
const request = require('request');
const async = require('async');

if (process.env.stage == 'production') {
    console.log("Running Environment: Production");
} else {
    console.log("Running Environment: Development");
}

const tasksLimit = process.env.tasksLimit || 10;

console.log('Version: ', require('./package.json').version);
console.log('tasksLimit: ', tasksLimit);

function handler (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;
    var body = event.body;

    if (typeof body == 'string') {
        body = JSON.parse(body);
    }
    console.log(JSON.stringify(body, null, 4));

    const PAGE_ACCESS_TOKEN = body.pageToken;
    const recipient_id = body.recipientId;
    const message = body.message;

    var totalSent = 0;
    var totalFail = 0;
    var failRecipientIds = [];

    if (Array.isArray(recipient_id)) {
        async.eachLimit(recipient_id, tasksLimit, function (recipientId, callback) {
            console.log('Sending message to ', recipientId);

            _sendMessage(PAGE_ACCESS_TOKEN, recipientId, message, function (err, result) {
                if (err) console.log(err);

                if (result && result.status) {
                    if (result.status == 'done') {
                        totalSent++;
                    }
                    if (result.status == 'fail') {
                        totalFail++;
                        failRecipientIds.push(recipientId);
                    }
                }

                callback();
            });

        }, function (err) {
            if (err) console.log(err);
            callback(null, {
                "status": "done",
                "failList": failRecipientIds,
                "totalSent": totalSent,
                "totalFail": totalFail
            });
        });
    } else {
        //request({
        //    method: 'POST',
        //    url: "https://graph.facebook.com/v2.6/me/messages?access_token=" + PAGE_ACCESS_TOKEN,
        //    headers: {
        //        'content-type': 'application/json'
        //    },
        //    body: {
        //        "recipient": {
        //            "id": recipient_id
        //        },
        //        "message": message
        //    },
        //    json: true
        //}, function (err, res, body) {
        //
        //    try {
        //        if (typeof body == 'string') {
        //            body = JSON.parse(body);
        //        }
        //        if (err) {
        //            console.log(err);
        //            //callback("Error occur when sending message to " + recipient_id);
        //            callback(null, {
        //                "status": "fail",
        //                "recipientId": recipient_id
        //            });
        //        } else if (res.body.error) {
        //            console.log(res.body.error);
        //            //callback("Error occur when sending message to " + recipient_id);
        //            callback(null, {
        //                "status": "fail",
        //                "recipientId": recipient_id
        //            });
        //        } else {
        //            console.log("Notification sent to: ", recipient_id);
        //            callback(null, {
        //                "status": "done",
        //                "recipientId": recipient_id
        //            });
        //        }
        //    } catch (err) {
        //        callback(null, {
        //            "status": "fail",
        //            "recipientId": recipient_id
        //        });
        //    }
        //
        //});
        _sendMessage(PAGE_ACCESS_TOKEN, recipient_id, message, callback);
    }
}

function _sendMessage (pageToken, recipient_id, message, callback) {
    request({
        method: 'POST',
        url: "https://graph.facebook.com/v2.6/me/messages?access_token=" + pageToken,
        headers: {
            'content-type': 'application/json'
        },
        body: {
            "recipient": {
                "id": recipient_id
            },
            "message": message
        },
        json: true
    }, function (err, res, body) {

        try {
            if (typeof body == 'string') {
                body = JSON.parse(body);
            }
            if (err) {
                console.log(err);
                console.log("Error occur when sending message to " + recipient_id);
                callback(null, {
                    "status": "fail",
                    "recipientId": recipient_id
                });
            } else if (res.body.error) {
                console.log(res.body.error);
                console.log("Error occur when sending message to " + recipient_id);
                callback(null, {
                    "status": "fail",
                    "recipientId": recipient_id
                });
            } else {
                console.log("Notification sent to: ", recipient_id);
                callback(null, {
                    "status": "done",
                    "recipientId": recipient_id
                });
            }
        } catch (err) {
            console.log("Error occur when sending message to " + recipient_id);
            callback(null, {
                "status": "fail",
                "recipientId": recipient_id
            });
        }

    });
}

module.exports.handler = handler;