const index = require('../index');

index.handler({
    "body": {
        "pageToken": "EAAB72NZCxPBkBAAiCzD2Io7uBvZBlp5z7D0J6uivC0aupw93cTJI0NVtWQlsxntnXNo1zOFdsusewJqcNfKhWC5Mc8ulGqv3fhYjAMElB8jzr1y7uKW6Iolv89aZC5RBlYIICXcsoVcOvaqfdZB0iSGrCsPKV4VX3PNrfRTbMgZDZD",
        "recipientId": ["1551518644921967", "2345671111111", "3345671111111", "4345671111111"],
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": [
                        {
                            "title": "adidas Group Training",
                            "subtitle": "現逢於星期二、三、五晚上，參加者可自組6-12 人的團隊預約專業體適能教練進行有系統的訓練。",
                            "image_url": "https://firebasestorage.googleapis.com/v0/b/temp-392a5.appspot.com/o/public%2FGroup%20Training.jpg?alt=media&token=0a651545-99f1-426c-99f4-b36fca5dcbea",
                            "buttons": [
                                {
                                    "type": "web_url",
                                    "title": "報名",
                                    "url": "http://www.adidas.com.hk/sportsbase/?utm_source=facebook&utm_medium=chatbot&utm_campaign=sportsbase#/training/"
                                },
                                {
                                    "type": "postback",
                                    "title": "了解更多",
                                    "payload": "sb1_faqs"
                                },
                                {
                                    "type": "element_share"
                                }
                            ]
                        }
                    ]
                }
            }
        }
    }
}, {}, function () {
    console.log(arguments);
});